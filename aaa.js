// import WaveSurfer from 'wavesurfer.js';

window.onload = function() {
  var WaveSurfer = require('wavesurfer.js');
  var wavesurfer = WaveSurfer.create({
    container: document.querySelector('#waveform'),
    waveColor: 'hotpink',
    plugins: [
      WaveSurfer.cursor.create({
          showTime: true,
          opacity: 1,
          customShowTimeStyle: {
              'background-color': '#000',
              color: '#fff',
              padding: '2px',
              'font-size': '10px'
          }
      })
    ],
    progressColor: 'orange',
    scrollParent: true
  });

  audio_file.onchange = function () {
    let files = this.files;
    let file = URL.createObjectURL(files[0]);

    var slider = document.querySelector('#slider');

    slider.oninput = function () {
        var zoomLevel = Number(slider.value);
        wavesurfer.zoom(zoomLevel);
    };
    wavesurfer.load(file);
  };

  wavesurfer.on('ready', function () {
    wavesurfer.play();
  });

  $("#btnStartPause").click(
    play = function () {
        wavesurfer.playPause();
    }
  );
};
