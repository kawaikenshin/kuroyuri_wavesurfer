var http = require('http');
var fs = require('fs');

const hostname = '127.0.0.1';
const port = 3000;

var server = http.createServer();
server.on('request', doRequest);
server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  });

// リクエストの処理
function doRequest(req, res) {
    fs.readFile('./index.html', 'UTF-8', 
        function(err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            res.end();
        });
}

